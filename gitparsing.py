#
# Copyright 2017 Paul Adams <paul@baggerspion.net>
# Copyright 2018 Kevin Ottens <ervin@ipsquad.net>
#
# The authors license this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from datetime import datetime
from pytz import utc
from typing import List, Tuple

import os
import pandas
import subprocess
import importlib
import glob
import argparse
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("git")


class GitParser:
    def __init__(self):
        self.__paths = []
        self.__rulesets = []
        self.git_commit_fields = ['id', 'author_name', 'author_email', 'date', 'subject', 'body', 'files']

        rulesets_dir = os.path.dirname(__file__) + "/rulesets"
        for file in glob.glob(rulesets_dir + "/*.py"):
            module_name = os.path.splitext(os.path.basename(file))[0]
            module = importlib.import_module("rulesets.%s" % (module_name))
            self.__rulesets.append(module)

    @staticmethod
    def get_argument_parser() -> argparse.ArgumentParser:
        arg_parser = argparse.ArgumentParser(add_help=False)
        arg_parser.add_argument("paths", metavar="path", nargs="+",
                                help="Path of a git repository to process or of a directory containing git repositories")
        arg_parser.add_argument("-f", "--start",
                                help="Start date")
        arg_parser.add_argument("-u", "--end",
                                help="End date")
        return arg_parser

    def add_repository(self, path):
        if not isinstance(path, str):
            raise ValueError("String expected")

        abs_path = os.path.abspath(os.path.expanduser(path))
        git_path = os.path.join(abs_path, ".git")
        if not os.path.exists(git_path):
            raise ValueError("Git repository expected, no %s found" % git_path)

        self.__paths.append(abs_path)

    def add_repositories(self, paths):
        if isinstance(paths, str):
            self.__add_repositories([paths])
        else:
            self.__add_repositories(paths)

    def __add_repositories(self, paths):
        for path in paths:
            abs_path = os.path.abspath(os.path.expanduser(path))
            git_path = os.path.join(abs_path, ".git")

            if os.path.exists(git_path):
                self.add_repository(abs_path)
            elif os.path.isdir(abs_path):
                subpaths = list(map(lambda x: os.path.join(abs_path, x), os.listdir(abs_path)))
                for subpath in subpaths:
                    self.add_repositories(subpath)

    def get_log(self, start_date=None, end_date=None):
        entries = []
        for path in self.__paths:
            entries.extend(self.__create_log_entries(path, start_date, end_date))

        return pandas.DataFrame(entries, columns=self.git_commit_fields + ['repository'])

    def __create_log_entries(self, path, start_date=None, end_date=None):
        git_log_format_options = {
            "id": "%H",
            "author_name": "%an",
            "author_email": "%ae",
            "date": "%ad",
            "subject": "%s",
            "body": "%b"}

        # the list of files is appended unconditionally (due to --name-only), so skip 'files' here
        git_log_fields = "%x1f".join([git_log_format_options[option] for option in self.git_commit_fields if option != 'files'])

        # use '1e' as start and '1f' as field separator
        git_log_format_string = "%x1e" + git_log_fields + "%x1f"

        command = "git --git-dir %s/.git log" % (path)
        command += " --date-order --reverse --all --date=iso --name-only"
        command += " --pretty=format:%s" % (git_log_format_string)

        if start_date:
            command += " --since %s" % (start_date)

        if end_date:
            command += " --until %s" % (end_date)

        logger.debug("Running git log: '%s'", command)
        log = self.__run_command(command)

        # Process the log into a list
        log = log.strip("\n\x1e").split("\x1e")
        log = [row.strip().split("\x1f") for row in log]
        log = [dict(zip(self.git_commit_fields, row)) for row in log]

        # Filter the log
        start_datetime = None
        if start_date:
            start_datetime = datetime.strptime(start_date, "%Y-%m-%d")

        end_datetime = None
        if end_date:
            end_datetime = datetime.strptime(end_date, "%Y-%m-%d")

        if start_date or end_date:
            log = list(filter(lambda x: self.__is_entry_acceptable(x, start_datetime, end_datetime), log))

        log = list(map(lambda x: self.__postprocess_entry(os.path.basename(path), x), log))

        return log

    def __run_command(self, command):
        process = subprocess.Popen(command.split(" "),
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT,
                                   universal_newlines=False)
        (log, err) = process.communicate()

        if process.returncode != 0:
            raise OSError(log)

        return log.decode("utf-8", errors="replace")

    def __is_entry_acceptable(self, entry, start_datetime, end_datetime):
        for ruleset in self.__rulesets:
            if not ruleset.is_entry_acceptable(entry):
                return False

        try:
            entry_datetime = datetime.strptime(entry['date'], "%Y-%m-%d %H:%M:%S %z")
            entry_datetime = entry_datetime.astimezone(utc)

            # Sometimes git gives us entries from the wrong date range
            if start_datetime and entry_datetime.date() < start_datetime.date():
                return False

            if end_datetime and entry_datetime.date() > end_datetime.date():
                return False
        except KeyError:
            return False

        return True

    def __postprocess_entry(self, repository, entry):
        try:
            files = entry['files'].strip("\n")
            files = files.split("\n")
            files = list(map(lambda x: "%s:%s" % (repository, x), files))
            entry['files'] = files
        except KeyError:
            entry['files'] = []

        entry['repository'] = repository
        entry['id'] = "%s:%s" % (repository, entry['id'])
        entry['date'] = datetime.strptime(entry['date'], "%Y-%m-%d %H:%M:%S %z")
        entry['date'] = entry['date'].astimezone(utc)

        for ruleset in self.__rulesets:
            ruleset.postprocess_entry(entry)

        return entry

    def process_gerrit_reviewers(self, log: pandas.DataFrame):
        """Adds a column, containing a list of reviewers.

        These must be in the commit message with the format "Reviewed-by: author <email>".
        """
        len_reviewed_by = len("Reviewed-by: ")
        def extract_reviewers(body: str) -> List[Tuple[str, str]]:
            reviewers = []
            reviewer_lines = [line for line in body.split('\n') if line.startswith("Reviewed-by: ")]
            for reviewer_line in reviewer_lines:
                reviewer_name, reviewer_email = reviewer_line[len_reviewed_by:].rsplit(maxsplit=1)
                reviewer_email = reviewer_email[1:-1]  # remove <> around email address
                reviewers.append((reviewer_name, reviewer_email))
            return reviewers

        log['reviewers'] = log.body.apply(extract_reviewers)
        return log
